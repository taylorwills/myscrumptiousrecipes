# Generated by Django 4.1.5 on 2023-01-17 22:41

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("recipes", "0004_ingredient"),
    ]

    operations = [
        migrations.RenameModel(
            old_name="Ingredient",
            new_name="RecipeIngredient",
        ),
    ]
